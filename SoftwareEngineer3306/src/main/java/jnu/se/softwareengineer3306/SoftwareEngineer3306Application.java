package jnu.se.softwareengineer3306;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author LovEK
 */
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan("jnu.se.softwareengineer3306")
public class SoftwareEngineer3306Application {

    public static void main(String[] args) {
        SpringApplication.run(SoftwareEngineer3306Application.class, args);
    }

}
