package jnu.se.softwareengineer3306.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: LovEK
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Value("${file.upload.absolutePath}")
    private String absolutePath;

    @Value("${file.upload.oppositePath}")
    private String oppositePath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(oppositePath + "**").
                addResourceLocations("file:/" + absolutePath);
    }

}
