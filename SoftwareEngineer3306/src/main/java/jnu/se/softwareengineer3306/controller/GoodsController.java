package jnu.se.softwareengineer3306.controller;

import jnu.se.softwareengineer3306.pojo.Goods;
import jnu.se.softwareengineer3306.pojo.ShowGoods;
import jnu.se.softwareengineer3306.pojo.goods.Book;
import jnu.se.softwareengineer3306.service.GoodsService;
import jnu.se.softwareengineer3306.service.UserService;
import jnu.se.softwareengineer3306.service.impl.GoodsServiceImpl;
import jnu.se.softwareengineer3306.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: LovEK
 */
@Controller
public class GoodsController {

    private final GoodsService goodsService;
    private final UserService userService;
    @Autowired
    GoodsController(GoodsServiceImpl goodsService , UserServiceImpl userService){
        this.goodsService = goodsService;
        this.userService = userService;
    }

    @Value("${file.upload.absolutePath}")
    private String absolutePath;

    @Value("${file.upload.oppositePath}")
    private String oppositePath;

    @RequestMapping({"index","/","index.html"})
    public String index(HttpSession session){
        List<Goods> allGoods = goodsService.list();
        Integer userId = (Integer) session.getAttribute("userId");
        // 展示商品时不要展示自己的商品
        allGoods.removeIf(goods -> goods.getUserId().equals(userId));

        List<ShowGoods> showGoodsList = new ArrayList<>();
        for (Goods goods : allGoods) {
            ShowGoods showGoods = new ShowGoods();

            showGoods.setGoodsId(goods.getGoodsId());
            showGoods.setGoodsName(goods.getGoodsName());
            showGoods.setGoodsPrice(goods.getGoodsPrice());
            showGoods.setUsername(userService.getById(goods.getUserId()).getUsername());
            showGoods.setImgUrl(goods.getImgUrl());
            showGoods.setUserId(goods.getUserId());

            showGoodsList.add(showGoods);
        }

        session.setAttribute("allGoods",showGoodsList);
        return "index";
    }

    /**
     * 我的商品页面
     */
    @RequestMapping("/myitem.html")
    public String myGoods(HttpSession session){
        List<Goods> myGoods = goodsService.listGoodsByUserId((Integer) session.getAttribute("userId"));
        session.setAttribute("myGoods",myGoods);
        return "/myitem";
    }

    /**
     * 添加商品页面
     */
    @RequestMapping("detail.html")
    public String pageToAdd(){
        return "detail";
    }

    /**
     * 添加商品
     */
    @PostMapping("/addGoods")
    public String addGoods(String goodsName, String goodsPrice,
                           @RequestPart("file") MultipartFile goodsImg,
                           HttpSession session) throws IOException {

        Goods goods = new Book();
        goods.setGoodsName(goodsName);
        goods.setGoodsPrice(new BigDecimal(goodsPrice));
        goods.setUserId((Integer) session.getAttribute("userId"));

        String originalFilename = goodsImg.getOriginalFilename();
        if(!goodsImg.isEmpty()){
            // 保存到本地
            goodsImg.transferTo(new File(absolutePath + originalFilename));
        }
        // DB保存相对路径
        goods.setImgUrl(oppositePath + originalFilename);
        goodsService.save(goods);

        return "redirect:/index";
    }

}
