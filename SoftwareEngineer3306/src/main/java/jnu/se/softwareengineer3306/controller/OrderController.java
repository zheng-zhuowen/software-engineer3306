package jnu.se.softwareengineer3306.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import jnu.se.softwareengineer3306.pojo.*;
import jnu.se.softwareengineer3306.service.GoodsService;
import jnu.se.softwareengineer3306.service.OrderService;
import jnu.se.softwareengineer3306.service.UserService;
import jnu.se.softwareengineer3306.service.impl.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: LovEK
 */
@Controller
public class OrderController {

    private final OrderService orderService;
    private final GoodsService goodsService;
    private final UserService userService;
    @Autowired
    OrderController(OrderServiceImpl orderService, GoodsService goodsService, UserService userService){
        this.orderService = orderService;
        this.goodsService = goodsService;
        this.userService = userService;
    }

    /**
     * 显示订单页面
     * @param session 将订单信息存到session中
     * @return 页面
     */
    @RequestMapping({"order","order.html"})
    public String order(HttpSession session){
        int userId = (Integer) session.getAttribute("userId");
        // 我卖出的
        List<Order> ordersBySellerId = orderService.listOrdersBySellerId(userId);
        // 转存到ShowOrders中用于显示在页面上
        List<ShowOrder> showSellOrders = ordersToShowOrders(ordersBySellerId);
        session.setAttribute("mySell",showSellOrders);

        // 我买入的
        List<Order> ordersByBuyerId = orderService.listOrdersByBuyerId(userId);
        List<ShowOrder> showBuyOrders = ordersToShowOrders(ordersByBuyerId);
        session.setAttribute("myBuy",showBuyOrders);
        return "order";
    }

    /**
     * 新增账单
     * @param goodsId 商品
     * @param buyerId 买家
     * @return 到订单页面
     */
    @RequestMapping("addOrder")
    private String addOrder(int goodsId, int buyerId){

        Order order = new Order();
        order.setGoodsId(goodsId);
        order.setBuyerId(buyerId);
        order.setSellerId(goodsService.getById(goodsId).getUserId());
        order.setOrderDate(new Date());
        orderService.save(order);

        return "redirect:/order";
    }

    /**
     * 处理确定收货/完成交易
     * @param orderId 要确定的订单ID
     * @return 重新回到订单页
     */
    @RequestMapping("ackReceive")
    public String ackReceive(int orderId){
        UpdateWrapper<Order> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("order_id",orderId).set("order_status", 2);
        orderService.update(updateWrapper);

        orderService.removeById(orderId);

        return "redirect:/order";
    }

    /**
     * 处理取消订单
     * @param orderId 要取消的订单ID
     * @return 重新回到订单页
     */
    @RequestMapping("cancelOrder")
    public String cancelOrder(int orderId){
        UpdateWrapper<Order> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("order_id",orderId).set("order_status", 0);
        orderService.update(updateWrapper);

        orderService.removeById(orderId);

        return "redirect:/order";
    }

    /**
     * 将原来pojo类型的订单转为可以显示在页面上的订单
     * @param orders 要转化的订单
     * @return 可以显示的订单
     */
    private List<ShowOrder> ordersToShowOrders(List<Order> orders) {
        List<ShowOrder> showOrders = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        for (Order order : orders) {
            // 取得订单对应的商品
            Goods goods = goodsService.getById(order.getGoodsId());
            // 商品对应的买卖家
            User buyer = userService.getById(order.getBuyerId());
            User seller = userService.getById(goods.getUserId());
            // 设置要显示的账单响应的属性
            ShowOrder showOrder = new ShowOrder();
            showOrder.setOrderId(order.getOrderId());
            showOrder.setGoodsName(goods.getGoodsName());
            showOrder.setGoodsPrice(goods.getGoodsPrice());
            showOrder.setBuyerName(buyer.getUsername());
            showOrder.setSellerName(seller.getUsername());
            showOrder.setOrderStatus(order.getOrderStatus());
            showOrder.setStringOrderDate(sdf.format(order.getOrderDate()));
            showOrder.setImgUrl(goods.getImgUrl());
            showOrders.add(showOrder);
        }
        return showOrders;
    }
}
