package jnu.se.softwareengineer3306.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import jnu.se.softwareengineer3306.pojo.User;
import jnu.se.softwareengineer3306.service.UserService;
import jnu.se.softwareengineer3306.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * @author: LovEK
 */
@Controller
public class UserController {

    private final UserService userService;
    @Autowired
    UserController(UserServiceImpl userService){
        this.userService = userService;
    }

    @GetMapping("/login")
    public String login(HttpSession session){
        session.removeAttribute("register_error");
        return "login";
    }

    @PostMapping("/loginFailure")
    public String loginFailure(HttpSession session){
        session.setAttribute("login_error","用户名不存在或密码错误");
        return "redirect:/login";
    }

    @PostMapping("/loginSuccess")
    public String loginSuccess(HttpSession session){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        int userId = userService.getUserIdByUsername(username);
        session.setAttribute("username",username);
        session.setAttribute("userId",userId);
        return "redirect:index";
    }

    @RequestMapping("/register")
    public String register(HttpSession session){
        session.removeAttribute("login_error");
        return "register";
    }

    @PostMapping("/registerHandle")
    public String registerHandle(String username, String password, String againPassword,HttpSession session){

        // 判空
        if ("".equals(username)){
            session.setAttribute("register_error","用户名为空");
            return "redirect:/register";
        }

        if ("".equals(password) || "".equals(againPassword)){
            session.setAttribute("register_error","密码为空");
            return "redirect:/register";
        }

        // 查找数据库是否有重名用户
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        User user = userService.getOne(wrapper);

        // 判断密码输入是否相同
        if (!Objects.equals(password, againPassword)){
            session.setAttribute("register_error","两次密码输入不一致");
            return "redirect:/register";
        }

        // 判断用户名是否存在
        if (user != null){
            session.setAttribute("register_error","用户名已存在");
            return "redirect:/register";
        }

        // 转发时处理注册成功处理器
        return "forward:/register_success";
    }

    @PostMapping("/register_success")
    public String registerSuccess(String username,String password){
        // 注册成功，调用service的save()方法可以直接将pojo存入数据库
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        userService.save(user);
        return "login";
    }

}
