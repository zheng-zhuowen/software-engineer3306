package jnu.se.softwareengineer3306.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import jnu.se.softwareengineer3306.pojo.Goods;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: LovEK
 */
@Mapper
public interface GoodsMapper extends BaseMapper<Goods> {
}
