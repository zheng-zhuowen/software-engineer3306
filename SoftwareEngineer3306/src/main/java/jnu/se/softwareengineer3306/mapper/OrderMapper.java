package jnu.se.softwareengineer3306.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import jnu.se.softwareengineer3306.pojo.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: LovEK
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {
}
