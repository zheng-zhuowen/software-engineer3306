package jnu.se.softwareengineer3306.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import jnu.se.softwareengineer3306.pojo.User;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author: LovEK
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
