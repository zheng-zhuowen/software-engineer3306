package jnu.se.softwareengineer3306.pojo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 订单类,记录了一条订单该有的信息
 * @author: LovEK
 */
@Component
@TableName(value = "`order`")
public class Order {
    /**
     * 订单编号
     */
    @TableId(type = IdType.AUTO)
    private Integer orderId;
    /**
     * 商品信息
     */
    private Integer goodsId;
    /**
     * 买方信息
     */
    private Integer buyerId;
    /**
     * 卖方信息
     */
    private Integer sellerId;
    /**
     * 下单时间
     */
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date orderDate;
    /**
     * 订单状态:默认创建订单时是准备中
     */
    private OrderStatus orderStatus = OrderStatus.PREPARE;

    public Order() {
    }

    public Order(Integer id, Integer goodsId, Integer buyerId, Date date, OrderStatus status) {
        this.orderId = id;
        this.goodsId = goodsId;
        this.buyerId = buyerId;
        this.orderDate = date;
        this.orderStatus = status;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", goodsId=" + goodsId +
                ", buyerId=" + buyerId +
                ", sellerId=" + sellerId +
                ", orderDate=" + orderDate +
                ", orderStatus=" + orderStatus +
                '}';
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Integer buyerId) {
        this.buyerId = buyerId;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }
}

