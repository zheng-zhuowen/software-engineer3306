package jnu.se.softwareengineer3306.pojo;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 描述订单状态
 * @author LovEK
 */
public enum OrderStatus {

    /**
     * CANCEL:0,表示订单由商家或者用户取消
     * PREPARE:1,表示订单准备中
     * COMPLETE:2,表示订单已完成
     */
    CANCEL(0, "已取消"),
    PREPARE(1, "准备中"),
    COMPLETE(2, "已完成");

    @EnumValue
    private Integer code;

    @JsonValue
    private String info;

    OrderStatus(Integer code, String info) {
        this.code = code;
        this.info = info;
    }

    @Override
    public String toString() {
        return "OrderStatus{" +
                "code=" + code +
                ", info='" + info + '\'' +
                '}';
    }

    public Integer getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setInfo(String info) {
        this.info = info;
    }

}
