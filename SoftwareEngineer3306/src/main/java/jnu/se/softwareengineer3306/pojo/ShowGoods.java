package jnu.se.softwareengineer3306.pojo;

import org.springframework.stereotype.Component;

/**
 * @author: LovEK
 */
@Component
public class ShowGoods extends Goods{

    private String username;

    public ShowGoods() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
