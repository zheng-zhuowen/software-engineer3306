package jnu.se.softwareengineer3306.pojo;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 用于存储要显示在页面上的订单信息
 * @author: LovEK
 */
@Component
public class ShowOrder extends Order {

    private String goodsName;
    private BigDecimal goodsPrice;
    private String buyerName;
    private String sellerName;
    private String imgUrl;
    private String stringOrderDate;

    public ShowOrder() {
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getStringOrderDate() {
        return stringOrderDate;
    }

    public void setStringOrderDate(String stringOrderDate) {
        this.stringOrderDate = stringOrderDate;
    }
}
