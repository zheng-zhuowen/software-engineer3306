package jnu.se.softwareengineer3306.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * 用户类，是所有用户的基类，在其基础上创建买家和卖家
 * @author: LovEK
 */
@Component
public class User {

    @TableId(type = IdType.AUTO)
    private Integer userId;
    private String username;
    private String password;

    /**
     * 我的商品
     */
    @TableField(exist = false)
    List<Goods> myGoods = new ArrayList<>();

    /**
     * 我的购买订单
     */
    @TableField(exist = false)
    List<Order> myBuyOrders = new ArrayList<>();

    /**
     * 我的销售订单
     */
    @TableField(exist = false)
    List<Order> mySellOrders = new ArrayList<>();

    public User() {
    }

    public User(Integer userId, String username, String password, List<Goods> myGoods, List<Order> myBuyOrders, List<Order> mySellOrders) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.myGoods = myGoods;
        this.myBuyOrders = myBuyOrders;
        this.mySellOrders = mySellOrders;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", myGoods=" + myGoods +
                ", myBuyOrders=" + myBuyOrders +
                ", mySellOrders=" + mySellOrders +
                '}';
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Goods> getMyGoods() {
        return myGoods;
    }

    public void setMyGoods(List<Goods> myGoods) {
        this.myGoods = myGoods;
    }

    public List<Order> getMyBuyOrders() {
        return myBuyOrders;
    }

    public void setMyBuyOrders(List<Order> myBuyOrders) {
        this.myBuyOrders = myBuyOrders;
    }

    public List<Order> getMySellOrders() {
        return mySellOrders;
    }

    public void setMySellOrders(List<Order> mySellOrders) {
        this.mySellOrders = mySellOrders;
    }
}
