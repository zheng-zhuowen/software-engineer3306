package jnu.se.softwareengineer3306.pojo.goods;

import jnu.se.softwareengineer3306.pojo.Goods;
import jnu.se.softwareengineer3306.pojo.User;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * Book类，其属性即Goods类的所有属性
 * @author: LovEK
 */
@Component
public class Book extends Goods {

    public Book() {
    }

    public Book(Integer goodsId, String goodsName, BigDecimal goodsPrice, Integer userId,String imgUrl) {
        super(goodsId, goodsName, goodsPrice, userId,imgUrl);
    }
}
