package jnu.se.softwareengineer3306.service;

import com.baomidou.mybatisplus.extension.service.IService;
import jnu.se.softwareengineer3306.pojo.Goods;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: LovEK
 */
@Service
public interface GoodsService extends IService<Goods> {

    /**
     * 获取某个用户所有的商品
     * @param userId 传入的用户ID
     * @return 该用户所有的商品
     */
    List<Goods> listGoodsByUserId(Integer userId);

}
