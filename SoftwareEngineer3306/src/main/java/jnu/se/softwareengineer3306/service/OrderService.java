package jnu.se.softwareengineer3306.service;

import com.baomidou.mybatisplus.extension.service.IService;
import jnu.se.softwareengineer3306.pojo.Order;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: LovEK
 */
@Service
public interface OrderService extends IService<Order> {

    /**
     * 根据卖家ID获取该卖家所有订单,用于“我卖出的”
     * @param sellerId 卖家ID
     * @return 该卖家的所有订单
     */
    List<Order> listOrdersBySellerId(Integer sellerId);

    /**
     * 根据买家ID获取该买家所有订单，用于“我买入的”
     * @param buyerId 买家ID
     * @return 该买家的所有订单
     */
    List<Order> listOrdersByBuyerId(Integer buyerId);

}
