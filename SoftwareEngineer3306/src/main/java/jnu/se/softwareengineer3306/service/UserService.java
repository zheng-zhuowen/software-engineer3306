package jnu.se.softwareengineer3306.service;


import com.baomidou.mybatisplus.extension.service.IService;
import jnu.se.softwareengineer3306.pojo.User;
import org.springframework.stereotype.Service;

/**
 * @author: LovEK
 */
@Service
public interface UserService extends IService<User> {

    /**
     * 通过用户名获取用户ID
     * @param username 用户名
     * @return 用户ID
     */
    int getUserIdByUsername(String username);

}
