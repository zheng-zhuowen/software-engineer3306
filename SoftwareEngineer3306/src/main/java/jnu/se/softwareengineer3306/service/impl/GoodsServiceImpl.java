package jnu.se.softwareengineer3306.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jnu.se.softwareengineer3306.mapper.GoodsMapper;
import jnu.se.softwareengineer3306.pojo.Goods;
import jnu.se.softwareengineer3306.service.GoodsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: LovEK
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper,Goods> implements GoodsService {

    @Override
    public List<Goods> listGoodsByUserId(Integer userId) {
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId);
        return baseMapper.selectList(queryWrapper);
    }
}
