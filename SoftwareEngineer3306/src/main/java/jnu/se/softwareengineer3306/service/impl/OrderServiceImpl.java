package jnu.se.softwareengineer3306.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jnu.se.softwareengineer3306.mapper.OrderMapper;
import jnu.se.softwareengineer3306.pojo.Order;
import jnu.se.softwareengineer3306.service.OrderService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: LovEK
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper,Order> implements OrderService {

    @Override
    public List<Order> listOrdersBySellerId(Integer sellerId) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("seller_id",sellerId);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<Order> listOrdersByBuyerId(Integer buyerId) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("buyer_id",buyerId);
        return baseMapper.selectList(queryWrapper);
    }
}
