package jnu.se.softwareengineer3306.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jnu.se.softwareengineer3306.mapper.UserMapper;
import jnu.se.softwareengineer3306.pojo.User;
import jnu.se.softwareengineer3306.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author: LovEK
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements UserService {

    @Override
    public int getUserIdByUsername(String username) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        User user = baseMapper.selectOne(wrapper);
        return user.getUserId();
    }
}
