# 创建数据库
DROP DATABASE IF EXISTS jnu_secondhand_shop ;
CREATE DATABASE jnu_secondhand_shop;
USE jnu_secondhand_shop;

# 创建user表
DROP TABLE IF EXISTS USER;
CREATE TABLE USER(
                     user_id INTEGER PRIMARY KEY AUTO_INCREMENT,
                     username VARCHAR(10),
                     PASSWORD VARCHAR(20)
);

# 创建goods表
DROP TABLE IF EXISTS goods;
CREATE TABLE goods(
                      goods_id INTEGER PRIMARY KEY AUTO_INCREMENT,
                      goods_name VARCHAR(255),
                      goods_price DECIMAL(10,2),
                      user_id INTEGER,
                      img_url VARCHAR(255),
                      FOREIGN KEY (user_id) REFERENCES USER(user_id)
);

# 创建order表
DROP TABLE IF EXISTS  `order`;
CREATE TABLE  `order`(
                         order_id INTEGER PRIMARY KEY AUTO_INCREMENT,
                         goods_id INTEGER,
                         buyer_id INTEGER,
                         seller_id INTEGER,
                         order_date DATETIME,
                         order_status INTEGER,
                         FOREIGN KEY(goods_id) REFERENCES goods(goods_id),
                         FOREIGN KEY(buyer_id) REFERENCES USER(user_id),
                         FOREIGN KEY(seller_id) REFERENCES goods(user_id)
);

# 向user表中插入初始数据
SET FOREIGN_KEY_CHECKS=0;
TRUNCATE TABLE USER;
SET FOREIGN_KEY_CHECKS=1;
INSERT INTO `user`(username,PASSWORD) VALUE('zzw','0000');
INSERT INTO `user`(username,PASSWORD) VALUE('zyh','0000');
INSERT INTO `user`(username,PASSWORD) VALUE('zhd','0000');
INSERT INTO `user`(username,PASSWORD) VALUE('zjf','0000');

# 向goods表中插入初始数据
SET FOREIGN_KEY_CHECKS=0;
TRUNCATE TABLE goods;
SET FOREIGN_KEY_CHECKS=1;
INSERT INTO goods VALUE(NULL,'高等数学',11.11,1,'/images/books/1.jpg');
INSERT INTO goods VALUE(NULL,'线性代数',11.11,1,'/images/books/2.jpg');
INSERT INTO goods VALUE(NULL,'离散数学',11.11,1,'/images/books/3.jpg');
INSERT INTO goods VALUE(NULL,'马克思主义基本原理',22.22,2,'/images/books/4.jpg');
INSERT INTO goods VALUE(NULL,'毛泽东思想',22.22,2,'/images/books/5.jpg');
INSERT INTO goods VALUE(NULL,'思想道德修养',22.22,2,'/images/books/6.jpg');
INSERT INTO goods VALUE(NULL,'计算机网络',33.33,3,'/images/books/7.jpg');
INSERT INTO goods VALUE(NULL,'计算机操作系统',33.33,3,'/images/books/8.jpg');
INSERT INTO goods VALUE(NULL,'数据结构',33.33,3,'/images/books/9.jpg');
INSERT INTO goods VALUE(NULL,'计算机组成原理',33.33,3,'/images/books/10.jpg');
INSERT INTO goods VALUE(NULL,'软件工程基础',33.33,4,'/images/books/11.jpg');
INSERT INTO goods VALUE(NULL,'软件工程与项目管理',33.33,4,'/images/books/12.jpg');