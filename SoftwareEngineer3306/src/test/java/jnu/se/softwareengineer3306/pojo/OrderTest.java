package jnu.se.softwareengineer3306.pojo;

import org.junit.jupiter.api.Test;

import java.util.Date;

/**
 * @author: LovEK
 */
class OrderTest{
    @Test
    void createOrderTest() {
        Order order = new Order(1,1,2, new Date(System.currentTimeMillis()), OrderStatus.COMPLETE);
        OrderStatus orderStatus = order.getOrderStatus();
        Integer num = orderStatus.getCode();
        String info = orderStatus.getInfo();
        System.out.println(num + ":" + info);
    }
}