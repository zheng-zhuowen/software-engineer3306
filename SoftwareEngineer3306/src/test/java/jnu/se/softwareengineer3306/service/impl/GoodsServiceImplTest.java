package jnu.se.softwareengineer3306.service.impl;

import jnu.se.softwareengineer3306.pojo.Goods;
import jnu.se.softwareengineer3306.pojo.goods.Book;
import jnu.se.softwareengineer3306.service.GoodsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author: LovEK
 */
@SpringBootTest
class GoodsServiceImplTest {

    @Autowired
    GoodsService goodsService;

    @Test
    void listGoodsByUserId() {
        List<Goods> goods = goodsService.listGoodsByUserId(1);
        goods.forEach(System.out::println);
    }

    @Test
    void save(){
        Goods goods = new Book();
        goods.setGoodsName("SpringBoot开发");
        goods.setGoodsPrice(new BigDecimal("33.33"));
        goods.setUserId(3);

        goodsService.save(goods);
    }

    @Test
    void list(){
        List<Goods> list = goodsService.list();
        for (Goods goods : list) {
            System.out.println(goods);
        }
    }
}