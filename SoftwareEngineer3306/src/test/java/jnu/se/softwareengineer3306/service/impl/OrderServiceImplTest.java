package jnu.se.softwareengineer3306.service.impl;

import jnu.se.softwareengineer3306.pojo.Order;
import jnu.se.softwareengineer3306.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

/**
 * @author: LovEK
 */
@SpringBootTest
class OrderServiceImplTest {

    @Autowired
    OrderService orderService;

    @Test
    void listOrdersBySellerId() {
        List<Order> list = orderService.listOrdersBySellerId(1);
        list.forEach(System.out::println);
    }

    @Test
    void listOrdersByBuyerId(){
        List<Order> list = orderService.listOrdersByBuyerId(2);
        list.forEach(System.out::println);
    }

    @Test
    void save() {
        Order order = new Order();
        order.setOrderDate(new Date(System.currentTimeMillis()));
        order.setGoodsId(2);
        order.setBuyerId(1);
        orderService.save(order);
    }
}