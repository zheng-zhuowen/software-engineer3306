package jnu.se.softwareengineer3306.service.impl;

import jnu.se.softwareengineer3306.pojo.User;
import jnu.se.softwareengineer3306.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author: LovEK
 */
@SpringBootTest
class UserServiceImplTest {
    @Autowired
    UserService userService;

    @Test
    void save(){
        User user = new User();
        user.setUsername("haha");
        user.setPassword("00");
        userService.save(user);
    }

    @Test
    void getById(){
        User user = userService.getById(1);
        System.out.println(user);
    }
}